var http = require('http');
var server = http.createServer(function( request, response ) {
	response.writeHead(200, {"Content-Type": "text/plain"});
  response.end("Hello World\n");
}); // to access this url i need to put basic auth.


// Listen on port 8000, IP defaults to 127.0.0.1
server.listen(80);


console.log("Basic Server loaded");